<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ApiController extends Controller
{
    public function wa(Request $request)
    {
        // return $request->all();
        $noHp   = $request->no_hp;
        $nim    = $request->nim;
        $pesan  = $request->pesan;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendwa/plain',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'username' => 'smsuntan_api',
                'password' => '5M0334s',
                'text' => $pesan,
                'GSM' => $noHp
            )
        ));
        $resp = curl_exec($curl);

        if ($resp) {

            $insert = DB::table('pesan')->insert([
                'nim'   => $nim,
                'tipe'  => 'wa',
                'no_hp' => $noHp,
                'pesan' => $pesan,
                'waktu' => date('Y-m-d H:i:s'),
            ]);

            return response()->json([
                'status' => true,
                'pesan' => 'terkirim'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'pesan' => 'tidak terkirim'
            ], 200);
        }

        curl_close($curl);
    }

    public function sms(Request $request)
    {
        $noHp   = $request->no_hp;
        $nim    = $request->nim;
        $pesan  = $request->pesan;

        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_RETURNTRANSFER => 1,
        //     CURLOPT_URL => 'http://ukt.keuangan.untan.ac.id/api.sms.php?kd_mhs=54695&handphone='.$noHp.'&isi='.str_replace(' ', '%20', $pesan),
        //     // CURLOPT_POST => true,
        //     // CURLOPT_POSTFIELDS => array(
        //     //     'username' => 'smsuntan_api',
        //     //     'password' => '5M0334s',
        //     //     'text' => $pesan,
        //     //     'GSM' => $noHp
        //     // )
        // ));
        // $response = curl_exec($curl);

        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://ukt.keuangan.untan.ac.id/api.sms.php?kd_mhs=54695&handphone='.$noHp.'&isi='.$pesan);
        $response = $request->getBody();

        // $response = file_get_contents('http://ukt.keuangan.untan.ac.id/api.sms.php?kd_mhs=54695&handphone='.$noHp.'&isi='.str_replace(' ', '%20', $pesan));

        $res = json_decode($response, true);

        if ($res[0]['Status'] == '0') {

            $insert = DB::table('pesan')->insert([
                'nim'   => $nim,
                'tipe'  => 'sms',
                'no_hp' => $noHp,
                'pesan' => $pesan,
                'waktu' => date('Y-m-d H:i:s'),
            ]);

            return response()->json([
                'status' => true,
                'pesan' => 'terkirim'
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'pesan' => 'tidak terkirim'
            ], 200);
        }

        // curl_close($curl);

    }

    public function data_kirim()
    {
        $datas = DB::table('pesan')->select(['no_hp', 'nim'])->get();

        $data = [
            'status' => true,
            'data' => $datas,
        ];

        return response()->json($data, 200);
    }
}
